import React, { Component } from 'react';
import Course from '../../models/Course';
import Utils from '../../utils';

const styles = require('../../styles/CourseList.module.css');

interface ListProps {
    courses: Course[];
    onCourseDeleted: () => void;
}

export default class CourseList extends Component<ListProps, {}> {
    constructor(props: ListProps) {
        super(props);

        this.printCardDecks = this.printCardDecks.bind(this)
        this.deleteCourse = this.deleteCourse.bind(this);
    }

    deleteCourse(id: number) {
        fetch(`https://localhost:44324/api/courses/${id}`, {
            method: 'DELETE'
        }).then(response => {
            if (response.ok) {
                this.props.onCourseDeleted();
            }
        });
    }

    printCardDecks(columns: number) {
        var courseChunks = Utils.chunkEntityArray(this.props.courses, columns);

        return (
            <React.Fragment>
                {courseChunks.map(courseArr => {
                    return (
                        <div className='card-deck mb-sm-4 mb-0'>
                            {courseArr.map(course => {
                                return (
                                    <div className={`card ${styles.courseCard}`} key={course.id}>
                                        <img className="card-img-top" src="https://designshack.net/wp-content/uploads/placeholder-image.png" alt="Course Image" />
                                        <div className="card-body">
                                            <h5 className="card-title">{course.name}</h5>
                                            <p className="card-text">
                                                {course.description}
                                            </p>
                                            <a href={"/courses/" + course.id} className="btn btn-primary">View Course</a>
                                        </div>
                                        <div className="card-footer">
                                            <p className="card-text">
                                                <small className="text-muted">{course.startDate}</small>
                                            </p>
                                        </div>
                                    </div>
                                )
                            })}
                        </div>
                    )
                })}
            </React.Fragment>
        )
    }

    render() {
        return (
            <React.Fragment>
                {this.printCardDecks(3)}
            </React.Fragment>
        )
    }
}