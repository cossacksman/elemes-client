import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, RouteComponentProps } from 'react-router-dom';
import Course from '../../models/Course';
import CourseList from './CourseList';
import CreateCourseForm from '../forms/CreateCourseForm';

const AppStyles = require('../../styles/Global.module.css');
const CoursesStyles = require('../../styles/Courses.module.css');

interface State {
    courses: Course[];
    loading: boolean;
}

export default class Courses extends Component<RouteComponentProps<{}>, State> {
    constructor(props: any) {
        super(props);

        this.state = {
            courses: [],
            loading: true
        }

        this.fetchCourseList = this.fetchCourseList.bind(this);
        this.courseCreated = this.courseCreated.bind(this);
        this.courseDeleted = this.courseDeleted.bind(this);
    }

    componentDidMount() {
        this.fetchCourseList();
    }

    fetchCourseList() {
        var request = new Request('https://localhost:44324/api/courses', {method: 'GET'})

        fetch(request)
            .then(response => response.json() as Promise<Course[]>)
            .then(data => {
                this.setState({ courses: data, loading: false });
            });
    }

    courseCreated() {
        this.fetchCourseList();
    }

    courseDeleted() {
        this.fetchCourseList();
    }

    render() {
        let courseContents = this.state.loading
            ? "Loading..."
            : <CourseList courses={this.state.courses} onCourseDeleted={this.courseDeleted} />

        return (
            <div className='row no-gutters'>
                <div className='col-lg-2 col-md-3 col-sm-12'>
                    <div className='py-3 pl-3 pr-3 pr-md-0'>
                        <CreateCourseForm onCourseCreated={this.courseCreated} />
                    </div>
                </div>
                <div className='col-lg-10 col-md-9 col-sm-12'>
                    <div className='p-3'>
                        {courseContents}
                    </div>
                </div>
            </div>
        );
    };
}
