import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, RouteComponentProps } from 'react-router-dom';
import Course from '../../models/Course';

const AppStyles = require('../../styles/Global.module.css');
const CourseStyles = require('../../styles/Course.module.css');

interface RouteProps {
    id: string
}

interface State {
    course: Course;
    loading: boolean;
}

export default class CoursePage extends Component<RouteComponentProps<RouteProps>, State> {
    constructor(props: any) {
        super(props);

        this.state = {
            course: {} as Course,
            loading: true
        }
    }

    componentDidMount() {
        const { id } = this.props.match.params;
        var request = new Request(`https://localhost:44324/api/courses/${id}`, {method: 'GET'})

        fetch(request)
            .then(response => response.json() as Promise<Course>)
            .then(data => {
                this.setState({ course: data, loading: false });
            });
    }

    public static renderCourse(course: Course) {
        return (
            <div className="jumbotron jumbotron-fluid">
                <div className="container">
                    <h1 className="display-4">{course.name}</h1>
                    <p className="lead">{course.description}.</p>
                </div>
            </div>
        );
    }

    render() {
        let courseContents = this.state.loading
            ? "Loading..."
            : CoursePage.renderCourse(this.state.course);

        return (
            <div className={CourseStyles.content}>
                {courseContents}
            </div>
        );
    };
}
