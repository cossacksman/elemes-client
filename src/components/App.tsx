import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Header from './header/Header';
import Home from './Home';
import Courses from './course/Courses';
import Users from './admin/Users';
import CoursePage from './course/Course';
import UserProfile from './user/UserProfile';
import Admin from './admin/Admin';

const AppStyles = require('../styles/Global.module.css');

class App extends Component {
  render() {
    return (
      <div className={AppStyles.App}>
        <Route path='/'>
          <Header />
          <Switch>
            <Route exact path='/' component={Home}/>

            <Route exact path='/courses' component={Courses}/>
            <Route path='/courses/:id' component={CoursePage}/>


            <Route exact path='/admin' component={Admin}/>
            <Route exact path='/admin/users' component={Users}/>
            <Route exact path='/user/:id' component={UserProfile}/>
          </Switch>
        </Route>
      </div>
    );
  }
}

export default App;
