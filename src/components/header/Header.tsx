import React, { Component } from 'react';
import Navigation from './Navigation';

const AppStyles = require('../../styles/Global.module.css');
const HeaderStyles = require('../../styles/Header.module.css');

export default class Header extends Component {
    render() {
        return (
            <Navigation />
        );
    }
}