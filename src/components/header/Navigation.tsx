import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import Users from '../admin/Users';

const AppStyles = require('../../styles/Global.module.css');
const HeaderStyles = require('../../styles/Header.module.css');
const NavigationStyles = require('../../styles/Navigation.module.css');

export default class Navigation extends Component {
    render() {
        return (
            <nav className='navbar navbar-expand-lg navbar-dark bg-primary'>
                <a className='navbar-brand' href='/'>Elemes</a>
                <button className='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarSupportedContent' aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'>
                    <span className='navbar-toggler-icon'></span>
                </button>
                <div className='collapse navbar-collapse' id='navbarSupportedContent'>
                    <ul className='navbar-nav mr-auto'>
                        <li className='nav-item'>
                            <NavLink exact to='/' className={NavigationStyles.navLink + " nav-link"} activeClassName='active'>Home</NavLink>
                        </li>
                        <li className={NavigationStyles.navLink + " nav-item dropdown"} >
                            <NavLink to='/courses' className='nav-link dropdown-toggle' activeClassName='active'
                               id='navbarDropdown'
                               role='button'
                               data-toggle='dropdown'
                               aria-haspopup='true'
                               aria-expanded='false'>
                                Courses
                            </NavLink>
                            <div className='dropdown-menu' aria-labelledby='navbarDropdown'>
                                <NavLink className='dropdown-item' to='/courses'>Course List</NavLink>
                                <NavLink className='dropdown-item' to='#'>Enrolled Courses</NavLink>
                                <div className='dropdown-divider'></div>
                                <NavLink className='dropdown-item' to='#'>Marketplace</NavLink>
                            </div>
                        </li>
                        <li className={NavigationStyles.navLink + " nav-item dropdown"} >
                            <NavLink to='/admin' className='nav-link dropdown-toggle' activeClassName='active'
                               id='navbarDropdown'
                               role='button'
                               data-toggle='dropdown'
                               aria-haspopup='true'
                               aria-expanded='false'>
                                Admin
                            </NavLink>
                            <div className='dropdown-menu' aria-labelledby='navbarDropdown'>
                                <NavLink className='dropdown-item' to='/admin/users'>Site Users</NavLink>
                                <NavLink className='dropdown-item' to='/admin/permissions'>Permissions</NavLink>
                                <div className='dropdown-divider'></div>
                                <NavLink exact className='dropdown-item' to='/admin'>Site Settings</NavLink>
                            </div>
                        </li>
                    </ul>
                    <form className='form-inline my-2 my-lg-0'>
                        <input className='form-control mr-sm-2' type='search' placeholder='Search' aria-label='Search'/>
                        <button className='btn btn-outline-light my-2 my-sm-0' type='submit'>Search</button>
                    </form>
                    <div className={NavigationStyles.profileButton + ' rounded-circle ml-4 mr-2'}>
                        <img className="rounded-circle img-thumbnail" src="http://www.landscapingbydesign.com.au/wp-content/uploads/2018/11/img-person-placeholder.jpg" />
                    </div>
                </div>
            </nav>
        );
    }
}