import React, { Component, FormEvent, ChangeEvent } from 'react';
import CourseCategory from '../../models/Category';
import Certificate from '../../models/Certificate';

const styles = require('../../styles/forms/CreateCourse.module.css');

interface FormState {
    name: string,
    description: string,
    category: CourseCategory,
    startDate: Date,
    endDate: Date,
    visible: boolean,
    published: boolean,
    certificate: Certificate,
    categories: CourseCategory[],
    certificates: Certificate[]
}

interface FormProps {
    onCourseCreated: () => void;
}

export default class CreateCourseForm extends Component<FormProps, FormState> {
    constructor(props: any) {
        super(props);

        this.state = {
            name: '',
            description: '',
            category: {} as CourseCategory,
            startDate: new Date(),
            endDate: new Date(),
            visible: true,
            published: false,
            certificate: {} as Certificate,
            categories: [],
            certificates: []
        };

        this.getCategories = this.getCategories.bind(this);
        this.getCertificates = this.getCertificates.bind(this);
        this.update = this.update.bind(this);
        this.submit = this.submit.bind(this);
    }

    componentDidMount() {
        this.getCategories();
        this.getCertificates();
    }

    getCategories() {
        var request = new Request('https://localhost:44324/api/categories', {method: 'GET'})

        fetch(request)
            .then(response => response.json() as Promise<CourseCategory[]>)
            .then(data => {
                this.setState({ categories: data });
            });
    }

    getCertificates() {
        var request = new Request('https://localhost:44324/api/certificates', {method: 'GET'})

        fetch(request)
            .then(response => {
                var res = response.json() as Promise<Certificate[]>;
                console.log(res);
                return res;
            })
            .then(data => {
                this.setState({ certificates: data });
            });
    }

    update(event: ChangeEvent<HTMLInputElement>) {
        event.preventDefault();

        let name = event.target.name;
        let value = event.target.value;

        let newState = Object.assign(this.state, {
            [name]: value
        });

        this.setState(newState);
    }

    submit(event: FormEvent<HTMLFormElement>) {
        event.preventDefault();

        let requestBody = {
            Name: this.state.name.trim(),
            Description: this.state.description.trim(),
            Category: this.state.category,
            StartDate: this.state.startDate,
            EndDate: this.state.endDate,
            Visible: this.state.visible,
            Published: this.state.published,
            Certificate: this.state.certificate
        } as Object;

        fetch('https://localhost:44324/api/courses', {
            method: 'PUT',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(requestBody)
        }).then(response => {
            if (response.ok) {
                console.log(response.json());
                this.props.onCourseCreated();
            }
        });
    }

    render() {
        return (
            <div className={styles.formContainer}>
                <form onSubmit={this.submit}>
                    <div className={`form-group ${styles.name}  ${styles.formTextField}`}>
                        <label>
                            Name
                            <input type='text'
                                className='form-control'
                                name='name'
                                placeholder='Course Name'
                                value={this.state.name}
                                onChange={this.update}
                                required />
                        </label>
                    </div>

                    <div className={`form-group ${styles.description} ${styles.formTextField}`}>
                        <label>
                            Description
                            <input type='text'
                                className='form-control'
                                name='description'
                                placeholder='Description'
                                value={this.state.description}
                                onChange={this.update} />
                        </label>
                    </div>

                    <div className={`form-group ${styles.category} ${styles.formSelectField}`}>
                        <label>
                            Category
                            <select name='category' className='custom-select'>
                                {this.state.categories.map(category => {
                                  return <option value={category.id} key={category.id}>{category.name}</option>
                                })};
                            </select>
                        </label>
                    </div>

                    <div className={`form-group ${styles.certificate} ${styles.formSelectField}`}>
                        <label>
                            Certificate
                            <select name='certificate' className='custom-select'>
                                {this.state.certificates.map(certificate => {
                                  return <option value={certificate.id} key={certificate.id}>{certificate.title}</option>
                                })}
                            </select>
                        </label>
                    </div>

                    <div className={`form-group ${styles.startDate} ${styles.formDateField}`}>
                        <label>
                            Start Date
                            <input type='datetime-local'
                                className='form-control'
                                name='startDate'
                                placeholder='Start Date'
                                value={this.state.startDate.toISOString().substring(0, 16)}
                                onChange={this.update}
                                required />
                        </label>
                    </div>

                    <div className={`form-group ${styles.endDate} ${styles.formDateField}`}>
                        <label>
                            End Date
                            <input type='datetime-local'
                                className='form-control'
                                name='endDate'
                                placeholder='End Date'
                                value={this.state.endDate.toISOString().substring(0, 16)}
                                onChange={this.update} />
                        </label>
                    </div>

                    <div className={`form-check ${styles.visible} ${styles.formCheckboxField}`}>
                        <label>
                            <input type='checkbox'
                                name='visible'
                                checked={this.state.visible}
                                onChange={this.update}
                                className={styles.checkbox} />
                            Visible
                        </label>
                    </div>

                    <div className={`form-check ${styles.published} ${styles.formCheckboxField}`}>
                        <label>
                            <input type='checkbox'
                                name='published'
                                checked={this.state.visible}
                                onChange={this.update}
                                className={styles.checkbox} />
                            Published
                        </label>
                    </div>

                    <div className={`${styles.submitButton} ${styles.formSubmitField}`}>
                        <input type='submit' value='Submit' className='btn btn-primary' />
                    </div>
                </form>
            </div>
        );
    }
}