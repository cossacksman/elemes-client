import React, { Component, FormEvent, ChangeEvent } from 'react';

const styles = require('../../styles/forms/CreateUser.module.css');

interface FormState {
    username: string;
    email: string;
    firstName: string;
    lastName: string;
    password: string;
    generatePassword: boolean;
}

interface FormProps {
    onUserCreated: () => void;
}

export default class CreateUserForm extends Component<FormProps, FormState> {
    constructor(props: any) {
        super(props);

        this.state = {
            username: '',
            email: '',
            firstName: '',
            lastName: '',
            password: '',
            generatePassword: false
        };

        this.reset = this.reset.bind(this);
        this.update = this.update.bind(this);
        this.submit = this.submit.bind(this);
    }

    reset() {
        this.setState({
            username: '',
            email: '',
            firstName: '',
            lastName: '',
            password: '',
            generatePassword: false
        })
    }

    update(event: ChangeEvent<HTMLInputElement>) {
        event.preventDefault();

        let name = event.target.name;
        let value = event.target.value;

        let newState = Object.assign(this.state, {
            [name]: value
        });

        this.setState(newState);
    }

    submit(event: FormEvent<HTMLFormElement>) {
        event.preventDefault();

        let requestBody = {
            UserName: this.state.username.trim().toLowerCase(),
            Email: this.state.email.trim().toLowerCase(),
            FirstName: this.state.firstName.trim(),
            LastName: this.state.lastName.trim(),
            Password: this.state.password.trim()
        } as Object;

        fetch('https://localhost:44324/api/users', {
            method: 'PUT',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(requestBody)
        }).then(response => {
            if (response.ok) {
                console.log(response.json());
                this.props.onUserCreated();
                this.reset();
            }
        });
    }

    render() {
        return (
            <div className={styles.formContainer}>
                <form onSubmit={this.submit}>
                    <div className={`form-group ${styles.username}  ${styles.formTextField}`}>
                        <label>
                            Username
                            <input type='text'
                                className='form-control'
                                name='username'
                                placeholder='Username'
                                value={this.state.username}
                                onChange={this.update}
                                autoComplete='username'
                                required />
                        </label>
                    </div>

                    <div className={`form-group ${styles.email} ${styles.formTextField}`}>
                        <label>
                            Email Address
                            <input type='email'
                                className='form-control'
                                name='email'
                                placeholder='Email'
                                value={this.state.email}
                                onChange={this.update}
                                autoComplete='email'
                                required />
                        </label>
                    </div>

                    <div className={`form-group ${styles.firstName} ${styles.formTextField}`}>
                        <label>
                            First Name
                            <input type='text'
                                className='form-control'
                                name='firstName'
                                placeholder='First Name'
                                value={this.state.firstName}
                                onChange={this.update}
                                autoComplete='given-name'
                                required />
                        </label>
                    </div>

                    <div className={`form-group ${styles.lastName} ${styles.formTextField}`}>
                        <label>
                            Last Name
                            <input type='text'
                                className='form-control'
                                name='lastName'
                                placeholder='Last Name'
                                value={this.state.lastName}
                                onChange={this.update}
                                autoComplete='family-name'
                                required />
                        </label>
                    </div>

                    <div className={`form-group ${styles.password} ${styles.formTextField}`}>
                        <label>
                            Password
                            <input type='password'
                                className='form-control'
                                name='password'
                                placeholder='Password'
                                value={this.state.password}
                                onChange={this.update}
                                autoComplete='new-password' />
                        </label>
                    </div>

                    <div className={`form-check ${styles.generatePassword} ${styles.formCheckboxField}`}>
                        <label>
                            <input type='checkbox'
                                name='generatePassword'
                                checked={this.state.generatePassword}
                                onChange={this.update}
                                className={styles.checkbox} />
                            Generate Password
                        </label>
                    </div>

                    <div className={`${styles.submitButton} ${styles.formSubmitField}`}>
                        <input type='submit' value='Submit' className='btn btn-primary' />
                    </div>
                </form>
            </div>
        );
    }
}