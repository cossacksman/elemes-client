import React, { Component, ChangeEvent, FormEvent } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import User from '../../models/User';
import CreateUserForm from '../forms/CreateUserForm';
import UserList from '../user/UserList';

const styles = require('../../styles/admin/AdminUser.module.css');

interface State {
    users: User[];
    loading: boolean;
}

export default class Users extends Component<RouteComponentProps<{}>, State> {
    constructor(props: any) {
        super(props);

        this.state = {
            users: [],
            loading: true
        };

        this.fetchUserList = this.fetchUserList.bind(this);
        this.userCreated = this.userCreated.bind(this);
        this.userDeleted = this.userDeleted.bind(this);
    }

    componentDidMount() {
        this.fetchUserList();
    }

    fetchUserList() {
        var request = new Request('https://localhost:44324/api/users', {method: 'GET'})

        fetch(request)
            .then(response => response.json() as Promise<User[]>)
            .then(data => {
                this.setState({ users: data, loading: false });
            });
    }

    userCreated() {
        this.fetchUserList();
    }

    userDeleted() {
        this.fetchUserList();
    }

    render() {
        var userList = this.state.loading
            ? <div>Loading...</div>
            : <UserList users={this.state.users} onUserDeleted={this.userDeleted} />;

        return (
            <div>
                <div className='row no-gutters'>
                    <div className='col-md-2 col-sm-3'>
                        <div className={styles.content}>
                            <CreateUserForm onUserCreated={this.userCreated} />
                        </div>
                    </div>
                    <div className='col-md-10'>
                        {userList}
                    </div>
                </div>
            </div>
        );
    };
}