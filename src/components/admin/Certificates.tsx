import React, { Component, ChangeEvent, FormEvent } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import Certificate from '../../models/Certificate';

const styles = require('../../styles/AdminCertificate.module.css');

interface State {
    certificates: Certificate[];
    loading: boolean;
}

export default class Certificates extends Component<RouteComponentProps<{}>, State> {
    constructor(props: any) {
        super(props);

        this.state = {
            certificates: [],
            loading: true
        };
    }

    render() {
        return <div>

        </div>
    }
}