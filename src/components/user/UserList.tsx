import React, { Component } from 'react';
import User from '../../models/User';

interface ListProps {
    users: User[];
    onUserDeleted: () => void;
}

export default class UserList extends Component<ListProps, {}> {
    constructor(props: ListProps) {
        super(props);

        this.deleteUser = this.deleteUser.bind(this);
    }

    deleteUser(username: string) {
        fetch(`https://localhost:44324/api/users/${username}`, {
            method: 'DELETE'
        }).then(response => {
            if (response.ok) {
                this.props.onUserDeleted();
            }
        });
    }

    render() {
        return (
            <table className="table table-hover table-striped">
                <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Username</th>
                        <th scope="col">First Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Options</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.users.map(user =>
                        <tr key={user.id}>
                            <td>{user.id}</td>
                            <td><a href={"/user/" + user.userName}>{user.userName}</a></td>
                            <td>{user.firstName}</td>
                            <td>{user.lastName}</td>
                            <td>{user.email}</td>
                            <td>
                                <a href='#'>Edit</a> <a href='#' onClick={() => this.deleteUser(user.userName)}>Delete</a>
                            </td>
                        </tr>
                    )}
                </tbody>
            </table>
        )
    }
}