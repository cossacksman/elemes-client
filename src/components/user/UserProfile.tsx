import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, RouteComponentProps } from 'react-router-dom';
import User from '../../models/User';

const AppStyles = require('../../styles/Global.module.css');
const ProfileStyles = require('../../styles/UserProfile.module.css');

interface RouteProps {
    id: string
}

interface State {
    user: User;
    loading: boolean;
}

export default class UserProfile extends Component<RouteComponentProps<RouteProps>, State> {
    constructor(props: any) {
        super(props);

        this.state = {
            user: {} as User,
            loading: true
        }
    }

    componentDidMount() {
        const { id } = this.props.match.params;
        var request = new Request(`https://localhost:44324/api/users/${id}`, {method: 'GET'})

        fetch(request)
            .then(response => response.json() as Promise<User>)
            .then(data => {
                this.setState({ user: data, loading: false });
            });
    }

    public static renderUser(user: User) {
        return (
            <div className="jumbotron jumbotron-fluid">
                <div className="container">
                    <h1 className="display-4">{user.firstName} {user.lastName}</h1>
                    <p className="lead">{user.email}.</p>
                </div>
            </div>
        );
    }

    render() {
        let userContents = this.state.loading
            ? "Loading..."
            : UserProfile.renderUser(this.state.user);

        return (
            <div className={ProfileStyles.content}>
                {userContents}
            </div>
        );
    };
}
