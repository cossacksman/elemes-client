import Course from "./models/Course";

export default class Utils {
    public static chunkEntityArray(array: Array<Course>, chunks: number){
        var results = [];
        
        while (array.length) {
            results.push(array.splice(0, chunks));
        }
        
        return results;
    }
}