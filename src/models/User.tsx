import Enrolment from "./Enrolment";

export default interface User {
    id: number;
    userName: string;
    email: string;
    firstName: string;
    lastName: string;
    password: string;
    enrolments: Enrolment[]
}