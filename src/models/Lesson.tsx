export default interface Lesson {
    id: number;
    title: string;
    description: string;
}