import Course from "./Course";

export default interface CourseCategory {
    id: number;
    name: string;
    parent: CourseCategory;
    categories: CourseCategory[];
    Courses: Course[];
}