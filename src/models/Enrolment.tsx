import User from "./User";
import Course from "./Course";

export default interface Enrolment {
    id: number;
    user: User;
    course: Course;
    start: Date;
    finish: Date;
}