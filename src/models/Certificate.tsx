import User from "./User";

export default interface Certificate {
    id: number;
    title: string;
    description: string;
    user: User;
    awarded: Date;
    validTo: Date;
    certificateNumber: string;
}