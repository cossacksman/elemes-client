import CourseCategory from './Category';

export default interface Course {
    id: number;
    name: string;
    description: string;
    Category: CourseCategory;
    startDate: Date;
    endDate: Date;
    visible: boolean;
    published: boolean;
}